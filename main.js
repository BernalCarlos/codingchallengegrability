/*********************************
	Main script for the applicacion
	All initialization, input callbacks, and application control, is here
**********************************/

/**
 * On document ready
 */
$(document).ready(function(){

	//Start the app
	restart();

	//Setup Events
	$(".subform > button").click(onButtonClicked);
});

/**
 * Handles the click event of all buttons in the applicacion
 */
function onButtonClicked(){

	switch ($(this).attr("id")) {

		case "send_input_btn":
			readInput();			
			break;

		default:
			restart();
			break;

	}
}

/**
 * Reads the input
 */
function readInput(){

	if ($("#input_textarea").val() == ''){
		return;
	}

	$("#form_output").show();
	$("#form_input button").hide();

	//Read input
	var input_lines = $("#input_textarea").val().split('\n');

	var current_input_line = 0;
	var num_test_cases = parseInt(input_lines[current_input_line].trim());

	for (var i = 0; i < num_test_cases; i++){

		current_input_line++;
		var matrix_size = input_lines[current_input_line].trim().split(" ")[0];
		var num_operations = input_lines[current_input_line].trim().split(" ")[1];
		
		var cube_summation = new CubeSummation(matrix_size, num_operations);

		for (var j = 0; j < num_operations; j++){
			current_input_line++;
			result_obj = cube_summation.execute_operation(input_lines[current_input_line].trim());
			
			if(result_obj.success == 1 && result_obj.result != undefined){
				addLineToTextArea(result_obj.result);
			}
		}
		
	}
}

/**
 * Restarts the applicacion
 */
function restart(){
	current_test_case = 0;

	$(".subform").hide();

	$("#form_input").show();
	$("#form_input button").show();

	$("#form_input textarea").val('');
	$("#form_output textarea").val('');

	$("#form_buttons").show();
}

/**
 * Adds a new line to the output text area
 */
function addLineToTextArea(text){
	textarea = $("#output_textarea");
	textarea_value = textarea.val();
	if(textarea_value != ''){
		textarea_value = textarea_value + '\n';
	}
	textarea.val(textarea_value + text);
}