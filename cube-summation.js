/*********************************
	CubeSummation Class (JS Prototype)
**********************************/

/**
 * Constructor
 * 
 * @param {[int]} t https://www.hackerrank.com/challenges/cube-summation
 * @param {[int]} n 
 * @param {[int]} m 
 */
function CubeSummation(n, m) {

	//Save construction values
    this.N = n;
    this.M = m;

    this.used_operations = 0

    //Create tha matrix
    this.matrix = [];
    for (var i = 0; i < this.N; i++) {

    	this.matrix.push( [] );
    	for (var j = 0; j < this.N; j++) {

    		this.matrix[i].push( [] );
    		for (var k = 0; k < this.N; k++) {
    			
    			this.matrix[i][j].push(0);
    		};
    	};
    };
}

/**
 * Executes an operation.
 * 
 * @param  {string} operation
 * 
 * @return {object} The return object has the following attributes:
 * "success" => 0 or 1
 * "result" => Result for the query
 * "error_code" => Error code in case "success" equals 0. This value can be 0 for "UNSUPORTED OPERATION" and 1 for No more operations available"
 * "error_msg" => Error message in case "success" equals 0.
 */
CubeSummation.prototype.execute_operation = function(operation) {

	//Split the operation
	operation = operation.split(" ");

	//Verify if there are operations available
	var return_obj = {"success": 0};

	if( !this.is_operation_available() ){
		return_obj.success = 0;
		return_obj.error_code = 1;
		return_obj.error_msg = "No more operations available";
		return return_obj;
	}

	//Execute Operation
	switch (operation[0].toLowerCase()){
		case "update":
			this.update(operation[1], operation[2], operation[3], parseInt(operation[4]));
			return_obj.success = 1;
			break;
		case "query":
			return_obj.result = this.query(operation[1], operation[2], operation[3], operation[4], operation[5], operation[6]);
			return_obj.success = 1;
			break;
		default:
			return_obj.success = 0;
			return_obj.error_code = 0;
			return_obj.error_msg = "UNSUPORTED OPERATION";
			break;
	}

	//Return the result
	return return_obj
}

/**
 * Returns true if the an operation is available (used_operations < M).
 * @return {Boolean}
 */
CubeSummation.prototype.is_operation_available = function(){
	if(this.used_operations < this.M){
		return true;
	}
	else{
		return false;
	}
}

/**
 * Updates the value of block (x,y,z) to W.
 * 
 * @param  {int} x https://www.hackerrank.com/challenges/cube-summation
 * @param  {int} y 
 * @param  {int} z 
 * @param  {int} w 
 */
CubeSummation.prototype.update = function(x, y, z, w) {
	this.matrix[(x-1)][(y-1)][(z-1)] = w;
	this.used_operations = this.used_operations + 1;
};

/**
 * calculates the sum of the value of blocks whose x coordinate is between x1 and x2 (inclusive),
 * y coordinate between y1 and y2 (inclusive) and z coordinate between z1 and z2 (inclusive).
 * 
 * @param  {int} x1 https://www.hackerrank.com/challenges/cube-summation
 * @param  {int} y1 
 * @param  {int} z1 
 * @param  {int} x2 
 * @param  {int} y2 
 * @param  {int} z2 
 * 
 * @return {int} The sum
 */
CubeSummation.prototype.query = function(x1, y1, z1, x2, y2, z2) {
    
    var sum = 0;
    for (var i = (x1-1); i <= (x2-1); i++) {
    	for (var j = (y1-1); j <= (y2-1); j++) {
    		for (var k = (z1-1); k <= (z2-1); k++) {
    			sum = sum + this.matrix[i][j][k];
    		};
    	};
    };

    this.used_operations = this.used_operations + 1;
    return sum;
};