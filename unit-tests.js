/*********************************
	Unit tests for the Cube Summation Model
**********************************/

/**
 * Verify if the cube is created correctly
 */
QUnit.test( "Cube creation", function( assert ) {

	var cube = new CubeSummation(4, 3);

  	assert.ok( cube.matrix.length == cube.N && cube.matrix[3].length == cube.N && cube.matrix[3][3].length == cube.N, "Cube size" );
  	assert.ok( cube.M == 3, "Number of operations" );

  	//Verify that all blocks are 0
  	var all_zero = true;

  	for (var i = 0; i < cube.N; i++) {
  		for (var j = 0; j < cube.N; j++) {
  			for (var k = 0; k < cube.N; k++) {
  				if( cube.matrix[i][j][k] != 0 ){
  					all_zero = false;
  				}
  			};
  		};
  	};

  	assert.ok( all_zero, "All blocks value is 0" );
});

/**
 * Verify if the UDPATE operation is working properly
 */
QUnit.test( "UDPATE operation", function( assert ) {

	var cube = new CubeSummation(4, 3);

	cube.update(1, 1, 4, 25);
	assert.ok( cube.matrix[0][0][3] == 25, "Block value at 1, 1, 4 must be 25 after UPDATE" );
});

/**
 * Verify if the QUERY operation is working properly
 */
QUnit.test( "QUERY operation", function( assert ) {

	var cube = new CubeSummation(4, 3);

	cube.update(1, 1, 4, 25);
	cube.update(1, 2, 3, 25);
	assert.ok( cube.query(1, 1, 1, 4, 4, 4) == 50, "Query from (1, 1, 1) to (4, 4, 4) must be 50" );
});

/**
 * Verify if the operations parser is working properly
 */
QUnit.test( "Operation parser", function( assert ) {

	var cube = new CubeSummation(4, 4);

	return_obj = cube.execute_operation("UPDATE 1 1 4 25");
	assert.ok( return_obj.success == 1, "UPDATE 1 1 4 25" );
	
	return_obj = cube.execute_operation("UPDATE 1 2 3 25");
	assert.ok( return_obj.success == 1, "UPDATE 1 2 3 25" );

	return_obj = cube.execute_operation("QUERY 1 1 1 4 4 4");
	assert.ok( return_obj.success == 1 && return_obj.result == 50, "QUERY 1 1 1 4 4 4" );

	return_obj = cube.execute_operation("BLABLA 1 1 1 4 4 4");
	assert.ok( return_obj.success == 0 && return_obj.error_code == 0, "UNSUPORTED OPERATION" );

	return_obj = cube.execute_operation("QUERY 1 1 1 4 4 4");
	return_obj = cube.execute_operation("QUERY 1 1 1 4 4 4");
	assert.ok( return_obj.success == 0 && return_obj.error_code == 1, "No more operations available" );
});

/**
 * Cube summation test case - https://www.hackerrank.com/challenges/cube-summation
 */
QUnit.test( "Cube summation test case", function( assert ) {

	//First test-case
	var cube = new CubeSummation(4, 5);

	return_obj = cube.execute_operation("UPDATE 2 2 2 4");
	assert.ok( return_obj.success == 1 && cube.matrix[1][1][1] == 4, "First test-case: UPDATE 2 2 2 4" );

	return_obj = cube.execute_operation("QUERY 1 1 1 3 3 3");
	assert.ok( return_obj.success == 1 && return_obj.result == 4, "First test-case: QUERY 1 1 1 3 3 3" );

	return_obj = cube.execute_operation("UPDATE 1 1 1 23");
	assert.ok( return_obj.success == 1 && cube.matrix[0][0][0] == 23, "First test-case: UPDATE 1 1 1 23" );

	return_obj = cube.execute_operation("QUERY 2 2 2 4 4 4");
	assert.ok( return_obj.success == 1 && return_obj.result == 4, "First test-case: QUERY 2 2 2 4 4 4" );

	return_obj = cube.execute_operation("QUERY 1 1 1 3 3 3");
	assert.ok( return_obj.success == 1 && return_obj.result == 27, "First test-case: QUERY 1 1 1 3 3 3" );

	//Second test-case
	var cube = new CubeSummation(2, 4);

	return_obj = cube.execute_operation("UPDATE 2 2 2 1");
	assert.ok( return_obj.success == 1 && cube.matrix[1][1][1] == 1, "Second test-case: UPDATE 2 2 2 1" );

	return_obj = cube.execute_operation("QUERY 1 1 1 1 1 1");
	assert.ok( return_obj.success == 1 && return_obj.result == 0, "Second test-case: QUERY 1 1 1 1 1 1" );

	return_obj = cube.execute_operation("QUERY 1 1 1 2 2 2");
	assert.ok( return_obj.success == 1 && return_obj.result == 1, "Second test-case: QUERY 1 1 1 2 2 2" );

	return_obj = cube.execute_operation("QUERY 2 2 2 2 2 2");
	assert.ok( return_obj.success == 1 && return_obj.result == 1, "Second test-case: QUERY 2 2 2 2 2 2" );
});